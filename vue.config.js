/**
 * @name vue.config
 * @description 系统开发配置
 * @author wk
 * @version 0.1.0
 * @date 2021/07/09
 */

module.exports = {
  // 配置转发代理
  devServer: {
    disableHostCheck: true,
    overlay: {
      warnings: true,
      errors: true
    },
    port: 8080,
    proxy: {
      '/': {
        target: "http://localhost:8080",
        ws: false, // 需要websocket 开启
        pathRewrite: {
          '^/': '/'
        }
      }
    }
  }
}