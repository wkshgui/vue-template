/**
 * @name website
 * @description 系统配置信息
 * @author wk
 * @version 0.1.0
 * @date 2021/07/08
 */
const website = {
  title: 'demo',
  copyright: 'Copyright © 2019 demo.com. All rights reserved.',
  key: 'demo', // 配置主键,目前用于存储
  whiteList: ['/login', '/404', '/401', '/lock'], // 配置无权限可以访问的页面
  whiteTagList: ['/login', '/404', '/401', '/lock'], // 配置不添加tags页面 （'/advanced-router/mutative-detail/*'——*为通配符）
  fistPage: {
    label: '首页',
    value: '/wel/index',
    params: {},
    query: {},
    group: [],
    close: false
  },
  // 配置菜单的属性
  menu: {
    props: {
      label: 'label',
      path: 'path',
      icon: 'icon',
      children: 'children'
    }
  }
}

export default website
