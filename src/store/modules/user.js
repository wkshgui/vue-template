import { getStore } from '@/utils/storage.js';

const user = {
  state: {
    userInfo: getStore({
      name: 'userInfo'
    }) || {},
    access_token: getStore({
      name: 'access_token'
    }) || '',
    refresh_token: getStore({
      name: 'refresh_token'
    }) || ''
  },
  mutations: {},
  actions: {}
};

export default user;
