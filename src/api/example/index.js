/**
 * @name example
 * @description api实例
 * @author wk
 * @version 0.1.0
 * @date 2021/07/09
 */
import request from '@/utils/request.js';

/**
 * @name exampleAPI
 * @description api实例接口
 * @author wk
 * @version 0.1.0
 * @date 2021/07/09
 */
export function exampleAPI(params){
  return request({
    url: '',
    method: '',
    params
  })
}