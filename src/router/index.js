import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    component: () => import(/* webpackChunkName: "home" */ '@/views/home/index.vue')
  }
];

const router = new VueRouter({
  mode: 'hash',
  routes
});

export default router;
